import java.util.Scanner;

public class PrimeNumberCheckerApp {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int bil, cek = 0;
        System.out.println("Menentukan Bilangan Prima");
        System.out.print("Masukkan Angka : ");

        bil=input.nextInt();
        System.out.println("===========================");
        for (int i = 2; i <= bil; i++){
            if (bil % i == 0){
                cek++;
            }
        }
        if (cek == 1) {
            System.out.println(bil+ " Adalah Bilangan Prima");
        }
        else  {
            System.out.println(bil+ " Bukan Bilangan Prima");
        }
    }
}
